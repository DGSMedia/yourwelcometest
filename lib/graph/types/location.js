import { GraphQLObjectType, GraphQLString } from 'graphql';

export default new GraphQLObjectType({
    name: 'Location',
    description: 'lat and lng of the property',
    fields: () => ({
      lat: {
        type: GraphQLString,
        description: 'latitude',
        resolve: ({ lat }) => lat,
      },
      lng: {
        type: GraphQLString,
        description: 'longitude',
        resolve: ({ lng }) => lng,
      },
    }),
  });