/*
import { sortBy } from 'lodash';

const simpleSort = ({ properties, sortKey = 'address', sortOrder = 'asc' }) => {
  console.log(sortOrder);
  return sortBy(properties, p => p[sortKey]);
};
*/

const simpleSort = ({ properties, sortKey = 'address', sortOrder = 'asc' }) => {
  const keys = [];
  const list = [];
  properties.forEach(element => keys.push(element[sortKey]));
  const sortedKeys = keys.sort(new Intl.Collator(undefined, { numeric: true, sensitivity: 'base' }).compare);
  sortedKeys.forEach((key) => {
    const result = properties.find(element => element[sortKey] === key);
    if (sortOrder === 'asc') list.push(result);
    else list.unshift(result);
  });
  return list;
};

export default simpleSort;
