import React from 'react';
import PropTypes from 'prop-types';
import Relay from 'react-relay';

import { GoogleMap, Marker, withGoogleMap } from 'react-google-maps';

const MyMapComponent = withGoogleMap(props =>
  (<GoogleMap
    defaultZoom={15}
    defaultCenter={{ lat: parseFloat(props.location.lat), lng: parseFloat(props.location.lng) }}
  >
    {props.isMarkerShown && <Marker
      position={{ lat: parseFloat(props.location.lat),
        lng: parseFloat(props.location.lng) }}
    />}
  </GoogleMap>));

export function PropertyPage({ relay, viewer }) {
  if (!viewer.property) {
    return (
      <div>
        <h3>this property ({relay.variables.propertyID}) was not found</h3>
      </div>
    );
  }

  return (
    <div>
      <h3>property: {viewer.property.address}</h3>
      <MyMapComponent
        key="map"
        isMarkerShown
        googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div className='fullHeight' />}
        containerElement={<div className='containerMap' />}
        mapElement={<div className='fullHeight' />}
        location={viewer.property.location}
      />
    </div>
  );
}

export default Relay.createContainer(PropertyPage, {
  initialVariables: {
    propertyID: null,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        property(propertyID: $propertyID) {
          address,
          id,
          location {
            lat,
            lng
          }
        }
      }
    `,
  },
});

PropertyPage.propTypes = {
  relay: PropTypes.any.isRequired,
  viewer: PropTypes.object.isRequired,
};
